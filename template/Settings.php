<div class="wrap">
    <h1>Facebook settings</h1>
    <form action="" method="post">
        <?php settings_fields('lx_fb_app'); ?>
        <?php do_settings_sections('lx_fb_app'); ?>
        <table class="form-table">
            <tr>
                <th><label for="lx_fb_app_id">Facebook app ID</label></th>
                <td><input class="regular-text" name="lx_fb_app_id" type="text" value="<?= get_option('lx_fb_app_id') ?>" /></td>
            </tr>
            <tr>
                <th><label for="lx_fb_app_secret">Facebook app secret</label></th>
                <td><input class="regular-text" name="lx_fb_app_secret" type="text" value="<?= get_option('lx_fb_app_secret') ?>" /></td>
            </tr>
            
            <tr>
                <th><label for="lx_fb_page_id">Facebook pagina ID</label></th>
                <td><input class="regular-text" name="lx_fb_page_id" type="text" value="<?= get_option('lx_fb_page_id') ?>" /></td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>
</div>

