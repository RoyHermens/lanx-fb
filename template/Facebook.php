<div class="facebookfeed-wrapper">
    <?php foreach($data['data'] as $post): ?>
        <?php if(self::hasContent($post)): ?>
        <div class="post-wrapper">
            <img src="<?= $this->getFullImage($post['id']) ?>" alt="" />
            <?php if(isset($post['message'])): ?>
                <div class="content">
                    <div class="timestamp">
                        Geplaatst op: <?= self::getDate($post['created_time']) ?>
                    </div>

                    <?= self::getMessage($post['message']) ?>

                    <?php if(strlen($post['message']) > 500): ?>
                        <div class="read-more">
                            <a href="<?= self::getUrl($post['id']) ?>" target="_blank">Lees meer</a>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

        </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>

