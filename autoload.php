<?php
spl_autoload_register(function($class){
    $base = __DIR__.'/classes/';
    if(strpos($class, '\\lx')){
        if(file_exists($base.str_replace('\\', '/', $class) . '.php')){
            require $base.str_replace('\\', '/', $class) . '.php';
        }else{
            return;
        }           
    }   
});

