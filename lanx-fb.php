<?php
/*
Plugin Name: LanX Facebook feed
Plugin URI: http://www.lanthopusx.nl
Author: LanthopusX
Version: 1.1
*/

// Autoload
require 'vendor/autoload.php';
require 'autoload.php';

add_action('wp_enqueue_scripts'  ,function(){
    wp_enqueue_style('fb-feed', plugin_dir_url(__FILE__).'/css/fb-feed.css');  
});

$container = Container\lxContainer::class;

// Admin
add_action('admin_menu', [$container::getInstance(Admin\lxAdminPage::class), 'init']);
// FB Shortcode
add_shortcode('lx_fb_feed', [$container::getInstance(Social\lxFacebook::class), 'displayFeed']);