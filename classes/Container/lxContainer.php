<?php

/**
 * Description of Container
 *
 * @author roy
 */
namespace Container;

class lxContainer {
    public static $container;
    public static function getInstance($class, $args = null)
    {
        if(isset(self::$container[$class])){
            return self::$container[$class];
        }
        return self::$container[$class] = new $class(($args ? $args : null));
    }
}
