<?php
namespace Admin;


/**
 * Description of lxAdminPage
 *
 * @author roy
 */
class lxAdminPage {
    public function init()
    {
        add_menu_page(__('LanX FB feed'), __('LanX FB feed'), 'manage_options', 'lx-fb-opt', [$this, 'view']);
        register_setting('lx_fb_app', 'lx_fb_app_id');
        register_setting('lx_fb_app', 'lx_fb_app_secret');
        register_setting('lx_fb_app', 'lx_fb_page_id');
    }
    public function view()
    {
        if($_SERVER['REQUEST_METHOD'] === 'POST'){
            update_option('lx_fb_app_id', $_POST['lx_fb_app_id']);
            update_option('lx_fb_app_secret', $_POST['lx_fb_app_secret']);
            update_option('lx_fb_page_id', $_POST['lx_fb_page_id']);
        }
        ob_start();
        require(plugin_dir_path(__FILE__).'../../template/Settings.php'); 
        $content = ob_get_clean();
        echo $content;
    }
}
