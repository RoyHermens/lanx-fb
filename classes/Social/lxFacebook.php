<?php

/**
 * Description of Facebook
 *
 * @author roy
 */
namespace Social;

class lxFacebook {
    protected $appId;
    protected $appSecret;
    public function __construct()
    {
        $this->appId = get_option('lx_fb_app_id');
        $this->appSecret = get_option('lx_fb_app_secret');
        $this->accessToken = $this->appId . '|' . $this->appSecret;
    }
    public function displayFeed($atts = null)
    {
        $atts = shortcode_atts([
            'amount' => 9
        ], $atts, 'lx_fb_feed');
        
        ob_start();
        $data = $this->getData((int)$atts['amount']);
        require(plugin_dir_path(__FILE__) .'../../template/Facebook.php');
        $content = ob_get_clean();
        return $content;
    }
    public function getData($count)
    {        
        $url = "https://graph.facebook.com/". get_option('lx_fb_page_id') ."/feed?access_token={$this->accessToken}&limit={$count}";
        $result = file_get_contents($url);
        
        $decoded = json_decode($result, true);
        return $decoded;
    }
    public static function hasContent($post)
    {
        if(isset($post['message'])){
            return true;
        } else{
            return false;
        }
    }
    public function getFullImage($id)
    {        
        $url = "https://graph.facebook.com/{$id}?fields=full_picture&access_token={$this->accessToken}";
        $result = file_get_contents($url);
        
        $decoded = json_decode($result, true);
        return $decoded['full_picture'];
    }
    public static function getTime($timestamp)
    {
        return substr($timestamp, strpos($timestamp, 'T') + 1, 5);
    }
    public static function getDate($timestamp)
    {      
        return substr($timestamp, 0, strpos($timestamp, 'T'));
    }
    public static function getMessage($message)
    {
        if(strlen($message) > 500){
            return substr($message, 0, 500) . '..';
        }
        return $message;
    }
    public static function getUrl($id)
    {
        $base = 'https://www.facebook.com/woonveghel/posts/';
        $id = substr($id, strpos($id, '_') + 1);
        $url = $base . $id;
        return $url;
    }
}
